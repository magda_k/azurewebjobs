﻿
namespace AzureMoviesConverter.Storage
{
    public class MovieBlobInformation
    {
        public string URL { get; set; }
        public string Extension { get; set; } 
        public string FileNameWithoutExtension { get; set; }
        public string URLWithoutExtension { get; set; }
        public string FileName { get; set; }
    }
}