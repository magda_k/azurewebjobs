﻿using System;
using System.Configuration;
using System.IO;
using System.Text;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Queue;
using Newtonsoft.Json;

namespace AzureMoviesConverter.Storage
{
    public static class StorageManager
    {
        private static CloudStorageAccount m_cStorageAccount;
        private static CloudBlobContainer m_cMovieBlobContainer;

        public static void Init()
        {
            var sConnectionString = ConfigurationManager.ConnectionStrings["AzureWebJobsStorage"].ConnectionString;
            var sBlobContainerName = ConfigurationManager.AppSettings["MoviesContainerName"];
            var sConvertedMoviesContainerName = ConfigurationManager.AppSettings["ConvertedMoviesContainerName"];

            try
            {
                m_cStorageAccount = CloudStorageAccount.Parse(sConnectionString);
                m_cMovieBlobContainer = CreateBlobContainer(sBlobContainerName);
                CreateBlobContainer(sConvertedMoviesContainerName);
                foreach (var qType in (QueueType[])Enum.GetValues(typeof(QueueType)))
                {
                    CreateQueue(qType);
                }
            }
            catch (StorageException ex)
            {
                System.Diagnostics.Debug.WriteLine("No connection with the Azure Cloud account");
            }
        }

        public static void CreateQueue(QueueType eQueueType)
        {
            string sQueueName = GetQueueName(eQueueType);
            CloudQueueClient queueClient = m_cStorageAccount.CreateCloudQueueClient();
            var moviesQueue = queueClient.GetQueueReference(sQueueName);
            moviesQueue.CreateIfNotExists();
        }

        public static CloudBlobContainer CreateBlobContainer(string sContainerName)
        {
            var blobClient = m_cStorageAccount.CreateCloudBlobClient();
            var blobContainer = blobClient.GetContainerReference(sContainerName);
            blobContainer.CreateIfNotExists();

            var permissions = blobContainer.GetPermissions();
            permissions.PublicAccess = BlobContainerPublicAccessType.Container;
            blobContainer.SetPermissions(permissions);

            return blobContainer;
        }


        public static void AddMessageToQueue(string sFileNameWithExtension, string sUrl, QueueType eQueueType)
        {
            var blobInfo = new MovieBlobInformation
            {
                FileName = sFileNameWithExtension,
                Extension = Path.GetExtension(sFileNameWithExtension).Replace(".", ""),
                FileNameWithoutExtension = Path.GetFileNameWithoutExtension(sFileNameWithExtension),
                URLWithoutExtension = Path.GetFileNameWithoutExtension(sUrl),
                URL = sUrl
            };

            var queueMessage = new CloudQueueMessage(JsonConvert.SerializeObject(blobInfo));
            string sQueueName = GetQueueName(eQueueType);

            CloudQueueClient queueClient = m_cStorageAccount.CreateCloudQueueClient();
            var queue = queueClient.GetQueueReference(sQueueName);
            queue.AddMessageAsync(queueMessage);
        }

        private static string GetQueueName(QueueType eQueueType)
        {
            string sQueueName = string.Empty;
            switch (eQueueType)
            {
                case QueueType.Webm:
                    sQueueName = ConfigurationManager.AppSettings["WebmQueueName"];
                    break;
                case QueueType.Mp4:
                    sQueueName = ConfigurationManager.AppSettings["Mp4QueueName"];
                    break;
                case QueueType.Ogg:
                    sQueueName = ConfigurationManager.AppSettings["OggQueueName"];
                    break;
            }
            return sQueueName;
        }

        public static string UploadFile(string sFileName, MemoryStream msFileStream, string sContentType)
        {
            var sUniqueBlobName = CreateFileUniqueBlobName(sFileName);
            CloudBlockBlob blockBlob = m_cMovieBlobContainer.GetBlockBlobReference(sUniqueBlobName);
            blockBlob.Properties.ContentType = sContentType;
            blockBlob.UploadFromStream(msFileStream);

            return blockBlob.Uri.ToString();
        }

        public static void DeleteFile(string sURL)
        {
            Uri blobUri = new Uri(sURL);
            string blobName = blobUri.Segments[blobUri.Segments.Length - 1];
            CloudBlockBlob blobToDelete = m_cMovieBlobContainer.GetBlockBlobReference(blobName);
            blobToDelete.Delete();
        }

        private static string CreateFileUniqueBlobName(string sFileName)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(Path.GetFileNameWithoutExtension(sFileName));
            sb.Append("-");
            sb.Append(Guid.NewGuid());
            sb.Append(Path.GetExtension(sFileName));

            return sb.ToString().Replace(" ", ".");
        }
    }
}
