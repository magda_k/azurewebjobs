﻿using System;
using System.IO;
using System.Net;
using System.Text;
using AzureMoviesConverter.Persistence;
using AzureMoviesConverter.Storage;
using Microsoft.Azure.WebJobs;
using Microsoft.WindowsAzure.Storage.Blob;
using NReco.VideoConverter;

namespace AzureMoviesConverter.WebJob
{
    public class Functions
    {
        private const string MOVIES_DIR = "movies\\";

        private const string CONTENT_TYPE_WEBM = "video/webm";
        private const string CONTENT_TYPE_MP4 = "video/mp4";
        private const string CONTENT_TYPE_OGG = "video/ogg";

        public static void ConvertToWebm(
            [QueueTrigger("%WebmQueueName%")] MovieBlobInformation blobInfo,
            [Blob("%MoviesContainerName%/{URLWithoutExtension}.{Extension}")] CloudBlockBlob inputBlob,
            [Blob("%ConvertedMoviesContainerName%/{URLWithoutExtension}.webm")] CloudBlockBlob outputBlob,
            TextWriter log)
        {
            Settings seSettings = new WebmSettings();
            ConvertAndUploadMovie(inputBlob, outputBlob, blobInfo.FileName, CONTENT_TYPE_WEBM, Format.webm, seSettings, log);
        }

        public static void ConvertToMp4(
            [QueueTrigger("%Mp4QueueName%")] MovieBlobInformation blobInfo,
            [Blob("%MoviesContainerName%/{URLWithoutExtension}.{Extension}")] CloudBlockBlob inputBlob,
            [Blob("%ConvertedMoviesContainerName%/{URLWithoutExtension}.mp4")] CloudBlockBlob outputBlob, string FileNameWithoutExtension, string Extension, TextWriter log)
        {
            Settings seSettings = new Mp4Settings();
            ConvertAndUploadMovie(inputBlob, outputBlob, blobInfo.FileName, CONTENT_TYPE_MP4, Format.mp4, seSettings, log);
        }


        public static void ConvertToOgg(
            [QueueTrigger("%OggQueueName%")] MovieBlobInformation blobInfo,
            [Blob("%MoviesContainerName%/{URLWithoutExtension}.{Extension}")] CloudBlockBlob inputBlob,
            [Blob("%ConvertedMoviesContainerName%/{URLWithoutExtension}.ogg")] CloudBlockBlob outputBlob, string FileNameWithoutExtension, string Extension, TextWriter log)
        {
            Settings seSettings = new OggSettings();
            ConvertAndUploadMovie(inputBlob, outputBlob, blobInfo.FileName, CONTENT_TYPE_OGG, Format.ogg, seSettings, log);
        }

        private static void ConvertAndUploadMovie(CloudBlockBlob inputBlob, CloudBlockBlob outputBlob,
            string sFileNameWithExtension, string sContentType, string sFormat, Settings seSettings, TextWriter log)
        {
            Logger logger = new Logger(log, sFormat.ToUpper(), sFileNameWithExtension);
            try
            {
                string sMoviesDirPath = AppDomain.CurrentDomain.BaseDirectory + MOVIES_DIR;
                string sTempFilePath = sMoviesDirPath + sFormat.ToUpper() + "_" + sFileNameWithExtension;

                CreateDirectoryIfNotExist(sMoviesDirPath, logger);

                using (var inputStream = inputBlob.OpenRead())
                {
                    SaveTemporaryFile(sTempFilePath, inputStream, logger);
                }

                using (var outputStream = new MemoryStream())
                {
                    ConvertToFormat(outputStream, sTempFilePath, sFormat, seSettings.NRecoConvertSettings, logger);

                    var sTempConvertedFilePath = sMoviesDirPath + Path.GetFileNameWithoutExtension(sFileNameWithExtension) + "." + sFormat;
                    SaveTemporaryFile(sTempConvertedFilePath, outputStream, logger);

                    outputBlob.Properties.ContentType = sContentType;
                    UploadConvertedBlob(outputBlob, outputStream, logger);
                }

                DeleteTemporaryFile(sTempFilePath, logger);
                InsertInfoIntoDatabase(inputBlob, outputBlob, sContentType);
            }
            catch (FFMpegException exf)
            {
                logger.LogErrorOnConsole("Error while converting!", exf);
            }
            catch (Exception ex)
            {
                logger.LogErrorOnConsole("Error while converting or uploading movie.", ex);
            }
        }

        private static void InsertInfoIntoDatabase(CloudBlockBlob inputBlob, CloudBlockBlob outputBlob, string sContentType)
        {
            EntityManager.InsertOrUpdateConvertedMovie(inputBlob.Uri.ToString(), sContentType, outputBlob.Uri.ToString());
        }

        private static void UploadConvertedBlob(CloudBlockBlob outputBlob, MemoryStream outputStream, Logger logger)
        {
            outputStream.Position = 0;
            logger.LogInfoOnConsole("Started uploading... ");
            outputBlob.UploadFromStream(outputStream);
            logger.LogInfoOnConsole("Ended uploading... ");
        }

        private static void ConvertToFormat(Stream outputStream, string sTempFilePath, string sFormat, ConvertSettings csConvertSettings, Logger logger)
        {
            logger.LogInfoOnConsole("Starting conversion... ");
            var ffMpeg = new FFMpegConverter();
            ffMpeg.ConvertMedia(sTempFilePath, null, outputStream, sFormat, csConvertSettings);
            logger.LogInfoOnConsole("Conversion finished. ");

            if (outputStream.Length == 0)
            {
                throw new FFMpegException(0, "Stream is empty!");
            }
        }

        private static void DeleteTemporaryFile(string sTempFilePath, Logger logger)
        {
            if (File.Exists(sTempFilePath))
            {
                File.Delete(sTempFilePath);
                logger.LogInfoOnConsole("Deleted temporary file. ");
            }
        }

        private static void SaveTemporaryFile(string sTempFilePath, Stream inputStream, Logger logger)
        {
            using (var file = new FileStream(sTempFilePath, FileMode.Create, FileAccess.Write))
            {
                inputStream.Position = 0;
                inputStream.CopyTo(file);
                logger.LogInfoOnConsole("Temporary file saved.");
            }
        }

        private static void CreateDirectoryIfNotExist(string sMoviesDirPath, Logger logger)
        {
            if (!Directory.Exists(sMoviesDirPath))
            {
                Directory.CreateDirectory(sMoviesDirPath);
                logger.LogInfoOnConsole("Created directory: " + sMoviesDirPath);
            }
        }
    }
}
