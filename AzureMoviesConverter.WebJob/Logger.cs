﻿using System;
using System.IO;
using System.Text;

namespace AzureMoviesConverter.WebJob
{
    public class Logger
    {
        private readonly TextWriter m_logger;
        private readonly string m_sFormat;
        private readonly string m_sFileNameWithExtension;


        public Logger(TextWriter logger, string sFormat = "", string sSFileNameWithExtension = "")
        {
            m_logger = logger;
            m_sFileNameWithExtension = sSFileNameWithExtension;
            m_sFormat = sFormat;
        }

        public void LogInfoOnConsole(string sMessage)
        {
            LogOnConsole("[INFO]", sMessage);
        }

        internal void LogErrorOnConsole(string sMessage, Exception ex = null)
        {
            LogOnConsole("[ERROR]", sMessage, ex);
        }

        private void LogOnConsole(string sAlter, string sMessage, Exception ex = null)
        {
            StringBuilder sbStringBuilder = new StringBuilder();

            sbStringBuilder.Append(sAlter);

            if (!string.IsNullOrWhiteSpace(m_sFormat))
            {
                sbStringBuilder.Append("[");
                sbStringBuilder.Append(m_sFormat);
                sbStringBuilder.Append("]");
            }

            if (!string.IsNullOrWhiteSpace(m_sFileNameWithExtension))
            {
                sbStringBuilder.Append("[");
                sbStringBuilder.Append(m_sFileNameWithExtension);
                sbStringBuilder.Append("]");
            }

            sbStringBuilder.Append(" ");
            sbStringBuilder.Append(sMessage);

            if (ex != null)
            {
                sbStringBuilder.AppendLine();
                sbStringBuilder.Append(ex.Message);
            }


            var sLogMessage = sbStringBuilder.ToString();
            m_logger.WriteLine(sLogMessage);
#if DEBUG
            Console.WriteLine(sLogMessage);
#endif
        }
    }
}
