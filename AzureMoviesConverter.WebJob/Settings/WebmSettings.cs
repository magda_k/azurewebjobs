﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMoviesConverter.WebJob
{
    public class WebmSettings : Settings
    {
        #region Const

        private const string VIDEO_CODEC_WEBM = "libvpx";
        private const string AUDIO_CODEC_WEBM = "libvorbis";

        #endregion

        #region Properties

        protected override string VideoCodec { get { return VIDEO_CODEC_WEBM; } }
        protected override string AudioCodec { get { return AUDIO_CODEC_WEBM; } }
        protected override int? Crt { get { return 5; } }
        protected override string OptionalParameters { get { return "-quality good -b:v 2M"; } }

        #endregion
    }
}
