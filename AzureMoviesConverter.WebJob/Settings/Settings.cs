﻿using System.Text;
using NReco.VideoConverter;

namespace AzureMoviesConverter.WebJob
{
    public abstract class Settings
    {
        #region Const
        //hd: 1280:720, fullHD: 1920:1080
        private const string VIDEO_SCALE = "480:270";

        //ffmpeg -pix_fmts
        private const string PIXEL_FORMAT = "yuv420p";
        #endregion

        #region Properties
        // ffmpeg -codecs
        protected abstract string VideoCodec { get; }
        protected abstract string AudioCodec { get; }

        //ffmpeg -pix_fmts
        protected virtual string PixelFormat { get { return PIXEL_FORMAT; } }
        protected virtual int? Crt { get { return null; } }

        //hd: 1280:720, fullHD: 1920:1080
        protected virtual string VideoScale { get { return VIDEO_SCALE; } }
        protected virtual string OptionalParameters { get { return string.Empty; } }

        public ConvertSettings NRecoConvertSettings { get; private set; }

        #endregion

        #region Constructors
        protected Settings()
        {
            NRecoConvertSettings = CreateConvertSettings();
        }

        #endregion

        #region Utils

        private ConvertSettings CreateConvertSettings()
        {
            StringBuilder sbBuilder = new StringBuilder();

            if (!string.IsNullOrWhiteSpace(this.VideoCodec))
            {
                sbBuilder.Append(" -vcodec ");
                sbBuilder.Append(this.VideoCodec);
            }

            if (!string.IsNullOrWhiteSpace(this.AudioCodec))
            {
                sbBuilder.Append(" -acodec ");
                sbBuilder.Append(this.AudioCodec);
            }

            sbBuilder.Append(" -pix_fmt ");
            sbBuilder.Append(this.PixelFormat);

            if (!string.IsNullOrWhiteSpace(this.OptionalParameters))
            {
                sbBuilder.Append(" ");
                sbBuilder.Append(this.OptionalParameters);
                sbBuilder.Append(" ");
            }

            if (this.Crt.HasValue)
            {
                sbBuilder.Append(" -crf ");
                sbBuilder.Append(this.Crt.Value);
            }

            sbBuilder.Append(" -movflags faststart ");          //to beginning fo the file

            sbBuilder.Append(" -s ");
            sbBuilder.Append(this.VideoScale);


            return new ConvertSettings { CustomOutputArgs = sbBuilder.ToString() };
        }
        #endregion
    }
}
