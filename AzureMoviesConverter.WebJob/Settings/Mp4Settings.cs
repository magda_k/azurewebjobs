﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMoviesConverter.WebJob
{
    public class Mp4Settings : Settings
    {
        #region Const

        private const string VIDEO_CODEC_MP4 = "libx264";
        private const string AUDIO_CODEC_MP4 = "libvo_aacenc";

        #endregion

        #region Properties

        protected override string VideoCodec { get { return VIDEO_CODEC_MP4; } }
        protected override string AudioCodec { get { return AUDIO_CODEC_MP4; } }
        protected override int? Crt { get { return 18; } }
        protected override string OptionalParameters { get { return "-profile:v baseline -preset slower"; } } 

        #endregion

    }
}
