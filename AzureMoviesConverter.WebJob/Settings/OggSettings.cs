﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMoviesConverter.WebJob
{
    public class OggSettings : Settings
    {
        #region Const

        private const string VIDEO_CODEC_OGG = "libtheora";
        private const string AUDIO_CODEC_OGG = "libvorbis";

        #endregion

        #region Properties

        protected override string VideoCodec {  get { return VIDEO_CODEC_OGG; } }
        protected override string AudioCodec { get { return AUDIO_CODEC_OGG; } }
        protected override string OptionalParameters { get { return "-q 5"; } }

        #endregion
    }
}
