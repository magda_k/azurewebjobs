﻿using System;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AzureMoviesConverter.Persistence;
using AzureMoviesConverter.Storage;
using AzureMoviesConverter.WebUI.Models;

namespace AzureMoviesConverter.WebUI.Controllers
{
    public class HomeController : Controller
    {
        private const int PLAYLIST_SIZE = 5;

        public ActionResult Index()
        {
            var model = new IndexViewModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Index(IndexViewModel model)
        {
            if (!ModelState.IsValid)
            {
                model.Message = MessageType.Failure;
                return View("Index", model);
            }

            HttpPostedFileBase hpfFile = model.File;

            try
            {
                UploadFileToStorage(hpfFile);
                model.Message = MessageType.Success;
            }
            catch (DbException exDb)
            {
                model.Message = MessageType.DatabaseError;
            }
            catch (Exception ex)
            {
                model.Message = MessageType.Failure;
            }

            return View("Index", model);
        }

        public ActionResult PlaylistAndPlayer()
        {
            var playlist = GetPlaylist();
            return PartialView("_PlaylistAndPlayer", playlist);
        }

        public ActionResult Playlist(string currentMovieURL = null)
        {
            var playlist = GetPlaylist(currentMovieURL);
            return PartialView("_Playlist", playlist);
        }

        public ActionResult GetPlayerSource(string currentMovieURL = null)
        {
            if (!string.IsNullOrWhiteSpace(currentMovieURL))
            {
                var currentMovie = EntityManager.GetMasterMovieByUrl(currentMovieURL);
                if (currentMovie != null)
                {
                    return Json(new ConvertedUrlDictionary(currentMovie), JsonRequestBehavior.AllowGet);
                }
            }

            return Json(HttpStatusCode.NotFound);
        }

        private static PlaylistModel GetPlaylist(string currentMovieURL = null)
        {
            PlaylistModel playlist = new PlaylistModel(PLAYLIST_SIZE);

            try
            {
                var lLatestConvertedMovies = EntityManager.GetLatestConvertedMovies(PLAYLIST_SIZE);
                if (lLatestConvertedMovies.Any())
                {
                    playlist.AddRange(lLatestConvertedMovies);

                    if (!string.IsNullOrWhiteSpace(currentMovieURL))
                    {
                        var currentMovie = playlist.FirstOrDefault(mm => mm.URL.Equals(currentMovieURL));
                        if (currentMovie != null)
                        {
                            currentMovie.Current = true;
                        }
                    }
                    else
                    {
                        playlist.First().Current = true;
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Db connection error");
            }

            return playlist;
        }

        private void UploadFileToStorage(HttpPostedFileBase hpfFile)
        {
            string sFileName = Path.GetFileName(hpfFile.FileName);
            string sContentType = hpfFile.ContentType;

            using (var msMovieStream = new MemoryStream())
            {
                hpfFile.InputStream.CopyTo(msMovieStream);
                msMovieStream.Position = 0;
                var sURL = StorageManager.UploadFile(sFileName, msMovieStream, sContentType);
                EntityManager.InsertOrUpdateMasterMovie(sFileName, sURL, sContentType);

                foreach (var qType in (QueueType[])Enum.GetValues(typeof(QueueType)))
                {
                    StorageManager.AddMessageToQueue(sFileName, sURL, qType);
                }
            }
        }

        public ActionResult UploadMessage(MessageType messageType)
        {
            return PartialView("_UploadMessage", messageType);
        }
    }
}