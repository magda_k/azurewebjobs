﻿using System.Collections.Generic;
using System.Linq;
using AzureMoviesConverter.Persistence;

namespace AzureMoviesConverter.WebUI.Models
{
    public class ConvertedUrlDictionary : Dictionary<string, string>
    {
        public ConvertedUrlDictionary(MasterMovie mm)
        {
            var lOrderedMovies =  mm.ConvertedMovies
                .OrderBy(cm => cm.ContentType.Equals("video/ogg"))
                .ThenBy(cm => cm.ContentType.Equals("video/webm"))
                .ThenBy(cm => cm.ContentType.Equals("video/mp4"));

            foreach (var convertedMovie in lOrderedMovies)
            {
                this.Add(convertedMovie.ContentType, convertedMovie.URL);
            }
        }
    }
}