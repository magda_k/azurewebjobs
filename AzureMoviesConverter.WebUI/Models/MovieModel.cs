﻿using System.Text;
using AzureMoviesConverter.Persistence;

namespace AzureMoviesConverter.WebUI.Models
{
    public sealed class MovieModel : MasterMovie
    {
        public bool Current { get; set; }
        public ConvertedUrlDictionary ContentTypeToUrlDictionaryOfConvertedMovies;

        public MovieModel(MasterMovie mm)
        {
            Title = mm.Title;
            URL = mm.URL;
            IsFinished = mm.IsFinished;
            ContentType = mm.ContentType;
            Id = mm.Id;
            Current = false;

            ContentTypeToUrlDictionaryOfConvertedMovies = new ConvertedUrlDictionary(mm);
        }

        public override string ToString()
        {
            StringBuilder sbBuilder = new StringBuilder();
            sbBuilder.Append("(");
            sbBuilder.Append(ContentType);
            sbBuilder.Append(") ");
            sbBuilder.Append(Title);
            return sbBuilder.ToString();
        }
    }
}