﻿using System.ComponentModel.DataAnnotations;
using System.Web;

namespace AzureMoviesConverter.WebUI.Models
{
    public class IndexViewModel
    {
        [Required(ErrorMessage = "Video cannot be empty")]
        [FileSize(50)] //MB
        [VideoFile]
        public HttpPostedFileBase File { get; set; }

        public MessageType Message { get; set; }

    }

    public enum MessageType
    {
        None = 0,
        Uploading = 1,
        Success = 2,
        Failure = 3,
        DatabaseError = 4,
    }
}