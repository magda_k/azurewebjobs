﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using AzureMoviesConverter.Persistence;

namespace AzureMoviesConverter.WebUI.Models
{
    public class PlaylistModel : IEnumerable<MovieModel>
    {
        private readonly int m_iPlaylistSize;
        private readonly List<MovieModel> m_lMovies;

        public PlaylistModel(int iMax)
        {
            m_iPlaylistSize = iMax;
            m_lMovies = new List<MovieModel>();
        }

        public PlaylistModel()
        {
            m_iPlaylistSize = 5;
            m_lMovies = new List<MovieModel>();
        }

        public void AddRange(List<MasterMovie> lMovies)
        {
            m_lMovies.Clear();
            var liMovieModels = lMovies.Take(m_iPlaylistSize).Select(mm => new MovieModel(mm));
            m_lMovies.AddRange(liMovieModels);
        }

        public IEnumerator<MovieModel> GetEnumerator()
        {
            return m_lMovies.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return m_lMovies.GetEnumerator();
        }

        public override string ToString()
        {
            StringBuilder sbBuilder = new StringBuilder();
            foreach (var movieModel in m_lMovies)
            {
                sbBuilder.AppendLine(movieModel.ToString());
            }
            return sbBuilder.ToString();
        }
    }
}