﻿

function getPlaylist() {
    var currentTab = $(".playlist-item[class*='current']");
    var sCurrentMovieURL =  currentTab.attr("movieurl");
    $('#playlist-partial-view').load('Home/Playlist' + '?currentMovieURL=' + sCurrentMovieURL, function () {
        $(".playlist-item").click(setupPlayer);
    });
}

function setCurrentTable(item) {
    $(".playlist-item").removeClass("current");
    item.addClass("current");
}

function setupPlayer() {
    setCurrentTable($(this));

    $.getJSON("Home/GetPlayerSource", { currentMovieURL: $(this).attr("movieurl") },
        function (data) {
            var videoContener = $(".video-contener");
            videoContener.empty();

            for (var contentType in data) {
                var url = data[contentType];
                videoContener.append('<source src=' + url + ' type=' + contentType + '>');
            }

            videoContener.attr({
                "autoplay": "autoplay"
            });
            videoContener.load();

        });
};

function setErrorClass(inputEditor) {
    var txbUpload = $("#input-upload");
    var sErrorClass = "input-validation-error";
    if (inputEditor.hasClass(sErrorClass)) {
        txbUpload.addClass(sErrorClass);
    } else {
        txbUpload.removeClass(sErrorClass);
    }
}

function showUploadMessageOnFormSubmit() {
    $("#uploadForm").submit(function () {
        if ($("#uploadForm").valid()) {
            $('#upload-message').load('Home/UploadMessage' + '?messageType=' + 1, function () {
                $('#upload-message').show();
            });
        }
    });
}

function clearFileChooser() {
    $("#File").change(function () {
        var inputValue = $(this).val();
        $("#input-upload").val(inputValue);
        $("#uploadForm").valid();
        setErrorClass($(this));
    });

}

function clearFileChooserOnClick() {
    $("#btClear").click(function () {
        var inputEditor = $("#File");
        $("#input-upload").val('');
        $("#input-upload").addClass("input-validation-error");
        inputEditor.replaceWith(inputEditor.val('').clone(true));
    }); 
}

function hideUploadMessageOnClick() {
    $("#upload-message").click(function () {
        $(this).hide();
    });

}

$(function() {
    $(".playlist-item").click(setupPlayer);

    hideUploadMessageOnClick();
    clearFileChooserOnClick();
    clearFileChooser();
    showUploadMessageOnFormSubmit();

    setInterval(function () { getPlaylist(); }, 5000);
});

//********* input file validation *************
$(function() {
    jQuery.validator.unobtrusive.adapters.add('filesize', ['maxmegabytes'], function(options) {
        var params = {
            maxmegabytes: options.params.maxmegabytes
        };

        options.rules['filesize'] = params;
        if (options.message) {
            options.messages['filesize'] = options.message;
        }
    });

    $.validator.addMethod('filesize', function(value, element, param) {
        var maxBytes = parseInt(param.maxmegabytes);
        if (element.files != undefined && element.files[0] != undefined && element.files[0].size != undefined) {
            var filesize = parseInt(element.files[0].size);
            return filesize <= maxBytes;
        }
        return true;
    });
}(jQuery));