﻿using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using AzureMoviesConverter.Storage;
using System;

namespace AzureMoviesConverter.WebUI
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            StorageManager.Init();
        }
    }
}
