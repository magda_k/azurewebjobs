﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;

namespace AzureMoviesConverter.WebUI
{
    public class FileSizeAttribute : ValidationAttribute, IClientValidatable
    {
        public int? MaxBytes { get; set; }

        public FileSizeAttribute(int iMaxMegaBytes)
            : base("Please upload a file of less than " + iMaxMegaBytes + " MB.")
        {
            MaxBytes = iMaxMegaBytes * 1024 * 1024;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata,
            ControllerContext context)
        {
            var rule = new ModelClientValidationRule
            {
                ValidationType = "filesize",
                ErrorMessage = FormatErrorMessage(metadata.DisplayName)
            };
            rule.ValidationParameters["maxmegabytes"] = MaxBytes;

            yield return rule;
        }

        public override bool IsValid(object value)
        {
            var htpFile = value as HttpPostedFileBase;
            if (htpFile == null)
            {
                return false;
            }

            if (htpFile.ContentLength > MaxBytes)
            {
                return false;
            }

            return true;
        }
    }
}
