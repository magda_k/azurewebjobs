﻿using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using System.Web;

namespace AzureMoviesConverter.WebUI
{
    class VideoFileAttribute : ValidationAttribute
    {
        public VideoFileAttribute()
            : base("Select a video file!")
        {
        }

        public override bool IsValid(object value)
        {
            var hpfFile = value as HttpPostedFileBase;
            if (hpfFile == null)
            {
                return false;
            }

            var regex = new Regex(@"video/*");
            Match match = regex.Match(hpfFile.ContentType);
            if (!match.Success)
            {
                return false;
            }

            return true;
        }
    }
}
