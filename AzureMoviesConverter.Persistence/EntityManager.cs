﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;

namespace AzureMoviesConverter.Persistence
{
    public static class EntityManager
    {
        public static MasterMovie GetMasterMovieByUrl(string sURL)
        {
            using (var _context = new MoviesConverterDb())
            {
                var query = from mm in _context.MasterMovies
                            where mm.URL.Equals(sURL)
                            select mm;
                return query.Include(cm => cm.ConvertedMovies).FirstOrDefault();
            }
        }

        public static List<MasterMovie> GetLatestConvertedMovies(int iMax)
        {
            using (var _context = new MoviesConverterDb())
            {
                var query = from mm in _context.MasterMovies
                            where mm.IsFinished
                            orderby mm.FinishedDate descending
                            select mm;
                return query.Include(cm => cm.ConvertedMovies).Take(iMax).ToList();
            }
        }

        public static List<MasterMovie> GetAllMasterMovies()
        {
            using (var _context = new MoviesConverterDb())
            {
                var query = from mm in _context.MasterMovies
                            orderby mm.CreatedDate descending
                            select mm;

                return query.ToList();
            }
        }

        public static bool CheckIfConversionIsFinished(string sURL)
        {
            using (var _context = new MoviesConverterDb())
            {
                var mmParentMovie = _context.MasterMovies.FirstOrDefault(mm => mm.URL.Equals(sURL));
                return mmParentMovie != null && mmParentMovie.IsFinished;
            }
        }

        public static void InsertOrUpdateMasterMovie(string sFileName, string sURL, string sContentType)
        {
            using (var _context = new MoviesConverterDb())
            {
                var mmMovie = _context.MasterMovies.FirstOrDefault(mm => mm.URL.Equals(sURL));

                if (mmMovie != null)
                {
                    mmMovie.Title = Path.GetFileNameWithoutExtension(sFileName);
                    mmMovie.ContentType = sContentType;
                    mmMovie.CreatedDate = DateTime.Now;
                }
                else
                {
                    _context.MasterMovies.Add(new MasterMovie
                    {
                        Title = Path.GetFileNameWithoutExtension(sFileName),
                        URL = sURL,
                        ContentType = sContentType,
                        CreatedDate = DateTime.Now,
                    });
                }

                _context.SaveChanges();
            }
        }

        public static void InsertOrUpdateConvertedMovie(string sParentURL, string sContentType, string sURL)
        {
            using (var _context = new MoviesConverterDb())
            {
                var mmParentMovie = _context.MasterMovies.FirstOrDefault(mm => mm.URL.Equals(sParentURL));
                var mmConvertedMovie = _context.ConvertedMovies.FirstOrDefault(mm => mm.URL.Equals(sURL));

                if (mmParentMovie != null)
                {
                    if (mmConvertedMovie != null)
                    {
                        mmConvertedMovie.ContentType = sContentType;
                        mmConvertedMovie.CreatedDate = DateTime.Now;
                        mmConvertedMovie.ParentMovie = mmParentMovie;
                    }
                    else
                    {
                        mmParentMovie.ConvertedMovies.Add(new ConvertedMovie
                        {
                            URL = sURL,
                            ContentType = sContentType,
                            CreatedDate = DateTime.Now,
                            ParentMovie = mmParentMovie,
                        });

                    }

                    _context.SaveChanges();
                }
            }
        }
    }
}
